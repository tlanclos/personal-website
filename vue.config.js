var RobotostxtPlugin = require("robotstxt-webpack-plugin").default;
var SitemapPlugin = require("sitemap-webpack-plugin").default;

module.exports = {
  configureWebpack: {
    plugins: [
      new RobotostxtPlugin({
        sitemap: "https://taylorlanclos.com/sitemap.xml",
        host: "taylorlanclos.com"
      }),
      new SitemapPlugin(
        "https://taylorlanclos.com",
        [
          {
            path: "/#/",
            priority: 0.8
          },
          {
            path: "/#/about",
            priority: 0.3
          },
          {
            path: "/#/resume",
            priority: 0.6
          },
          {
            path: "/#/projects",
            priority: 0.5
          }
        ],
        {
          lastMod: true,
          changeFreq: "monthly"
        }
      )
    ]
  }
};
